package com.colvengames.cupidliedme.customview.rangeseekbar.interfaces;

/**
 * Created by owais.ali on 7/14/2016.
 */
public interface OnSeekbarFinalValueListener {
    void finalValue(Number value);
}
