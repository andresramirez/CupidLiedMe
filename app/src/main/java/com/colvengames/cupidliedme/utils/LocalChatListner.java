package com.colvengames.cupidliedme.utils;

import com.colvengames.cupidliedme.model.ChatMessage;

/**
 * Created by Kaushal on 19-01-2018.
 */

public interface LocalChatListner {
    public void onLocalChat(ChatMessage chatMessage);
}
