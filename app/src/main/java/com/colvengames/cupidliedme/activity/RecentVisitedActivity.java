package com.colvengames.cupidliedme.activity;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.colvengames.cupidliedme.R;
import com.colvengames.cupidliedme.adapter.RecentVisitedAdapter;
import com.colvengames.cupidliedme.interfaces.OnItemClickListner;
import com.colvengames.cupidliedme.utils.BaseActivity;
import com.colvengames.cupidliedme.utils.GridSpacingItemDecoration;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecentVisitedActivity extends BaseActivity implements OnItemClickListner {

    @BindView(R.id.rvRecentVisited)
    RecyclerView rvRecentVisited;

    private RecentVisitedAdapter recentVisitedAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_visited);
        ButterKnife.bind(this);
        setAdapter();
    }

    public void setAdapter() {
        recentVisitedAdapter = new RecentVisitedAdapter(this, this);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        rvRecentVisited.setLayoutManager(mLayoutManager);
        rvRecentVisited.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        rvRecentVisited.setItemAnimator(new DefaultItemAnimator());
        rvRecentVisited.setAdapter(recentVisitedAdapter);
        rvRecentVisited.setNestedScrollingEnabled(false);
    }

    @Override
    public void onItemClick(int position, String value, int outerpos) {

    }
}
